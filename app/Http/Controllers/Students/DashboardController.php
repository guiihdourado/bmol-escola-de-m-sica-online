<?php

namespace Bmol\Http\Controllers\Students;

use Illuminate\Http\Request;

use Bmol\Http\Requests;
use Bmol\Http\Controllers\Controller;

class DashboardController extends Controller
{
	public function index()
	{
		return view('students.dashboard');
	}
}
