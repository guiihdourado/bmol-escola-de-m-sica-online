<?php

namespace Bmol\Interfaces;

interface AuthInterface
{
    public function create(array $data);
}