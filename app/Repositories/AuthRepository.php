<?php

namespace Bmol\Repositories;

use Bmol\Models\User;
use Bmol\Models\Person;

use Bmol\Interfaces\AuthInterface;

class AuthRepository implements AuthInterface
{
        protected $user;
        protected $person;
        
        public function __construct(User $user, Person $person)
        {
                $this->user = $user;
                $this->person = $person;
        }
        
        public function create(array $data)
        {       
                $person = $this->person->create(['name' => $data['name']]);
                $user = $this->user->create([
                        'person_id' => $person->id,
                        'email' => $data['email'],
                        'password' => bcrypt($data['password']),
                ]);

                return $user;
        }


}
?>