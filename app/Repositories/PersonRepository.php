<?php

namespace Bmol\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PersonRepository
 * @package namespace Bmol\Repositories;
 */
interface PersonRepository extends RepositoryInterface
{
    //
}
