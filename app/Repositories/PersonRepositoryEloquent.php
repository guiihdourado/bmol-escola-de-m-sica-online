<?php

namespace Bmol\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Bmol\Repositories\PersonRepository;
use Bmol\Models\Person;
use Bmol\Validators\PersonValidator;

/**
 * Class PersonRepositoryEloquent
 * @package namespace Bmol\Repositories;
 */
class PersonRepositoryEloquent extends BaseRepository implements PersonRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Person::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
