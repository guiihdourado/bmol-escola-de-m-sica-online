<?php

namespace Bmol\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace Bmol\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
