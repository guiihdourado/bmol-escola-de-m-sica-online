<?php

namespace Bmol\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Person extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'persons';
    protected $fillable = ['name', 'cnpj_cpf', 'isStudent', 'isInstructor'];

    public function users()
    {
    	return $this->hasMany(User::class);
    }

}
