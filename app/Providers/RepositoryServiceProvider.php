<?php

namespace Bmol\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'Bmol\Interfaces\AuthInterface',
             'Bmol\Repositories\AuthRepository'
        );

        $this->app->bind(
            'Bmol\Repositories\UserRepository',
             'Bmol\Repositories\UserRepositoryEloquent'
        );

        $this->app->bind(
            'Bmol\Repositories\PersonRepository',
             'Bmol\Repositories\PersonRepositoryEloquent'
        );
    }
}
